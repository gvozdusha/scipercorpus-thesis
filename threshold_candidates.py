# this script gives threshold-candidates

# the script detects an avarage threshold by given %-candidate that we set (or get from manual annotation calculating the number of word we annotate on avarage)
# way/idea of getting thresholds per cnc: x% of cnc's words above it (put these values to the cnc-table in the DB)
# with and without func or irrelevant words (areed to calculate without)


# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

import cnc_tools

import general_utils

# for sorting
import functools

# func for for sorting from max to mix
def reverse_numeric(x, y):
    return y - x

def get_cnc_max_cossim_list (cnc, cursor):
    cnc_max_cossim_list = []

    all_comp = []

    #cnc_len = get_cnc_length(cnc,cursor)
    cnc_dict = cnc_tools.get_cnc_dict (cnc, cursor)
    for comp in cnc_dict: 
        all_comp.append(cnc_tools.get_cnc_component_cossim_dict (comp[0], cursor))

    for i in range(len(all_comp[0])): # length of one sample
        max_value = 0.0
        for j in range(len (cnc_dict)): # cnc len
            max_value = max (max_value, all_comp[j][i][1])
        cnc_max_cossim_list.append (max_value)

    return cnc_max_cossim_list 

def get_cnc_threshold_by_percent(cnc, percent, without_func, cursor): # 'cutting 20% of constructed cnc_max_cossim_list' / order is not followed!

    temp_list = get_cnc_max_cossim_list(cnc, cursor)
    cnc_max_cossim_list = []

    # func words exclution if needed
    if without_func == 1:
        # cursor.execute("SELECT sample_word_is_func FROM similarities WHERE comp_word_id = " + str(cnc)) # only one sample copy is needed, so here comp_word_id=cnc as the first comp equivalent
        cursor.execute("SELECT sample_word_is_irrel FROM similarities WHERE comp_word_id = " + str(cnc)) # only one sample copy is needed, so here comp_word_id=cnc as the first comp equivalent

        rows = cursor.fetchall()

        for i in range(len(rows)):
            if rows[i][0] == 0: # word is not irrel
                cnc_max_cossim_list.append(temp_list[i])
                #print (i)
    else:
        cnc_max_cossim_list= temp_list # copy all the items
    #print (cnc_max_cossim_list)

    limit_percent = int(percent*len(cnc_max_cossim_list)) # number of items that reprecent 20% or less
    sorted_from_max_list = sorted(cnc_max_cossim_list, key=functools.cmp_to_key(reverse_numeric))

    return sorted_from_max_list[limit_percent-1] # returning the last item we want to 'include' so that threshold should be used with >=

def create_theshold_values_column (percent, cnc_list, without_func, cursor):
        cnc_threshold_values_list = []

        # adding new column - with % threshold
        cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_" + str(int(percent*100)) + " float(32) NOT NULL") 

        for cnc_id in cnc_list:
            #print(cnc_id)
            threshold = get_cnc_threshold_by_percent(cnc_id, percent, without_func, cursor)
            #cnc_threshold_dict.update({cnc_id: threshold_10}) # dict 'cnc:threshold_value'
            cnc_threshold_values_list.append(threshold)
            
            cursor.execute("UPDATE cnc SET threshold_" + str(int(percent*100)) +" = " + str(threshold)+ " WHERE word_id = "+ str(cnc_id))
            connection.commit()

        # threshold average
        #print (cnc_threshold_values_list)
        print(str(percent*100) +'% threshold average:') 
        print(general_utils.get_list_average (cnc_threshold_values_list))


if __name__ == '__main__':

    percent_list = [0.35, 0.23565] #[0.1, 0.15, 0.2, 0.25, 0.3] # these are the %-candidates we want to use for finding threshold candidated further 
    
    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()
        
        cnc_list = cnc_tools.get_all_cnc(cursor)
        print(len(cnc_list)) #5100

        for percent in percent_list:
            create_theshold_values_column (percent, cnc_list, 1, cursor)


# 10%

        # #cnc_threshold_dict = {}
        # cnc_threshold_values_list = [] # just to have only values printed

        # # adding new column - with 10% threshold
        # cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_10 float(32) NOT NULL") 

        # for cnc_id in cnc_list:
        #     #print(cnc_id)
        #     threshold_10 = get_cnc_threshold_by_percent(cnc_id, 0.1, 1, cursor)
        #     #cnc_threshold_dict.update({cnc_id: threshold_10}) # dict 'cnc:threshold_value'
        #     cnc_threshold_values_list.append(threshold_10)
            
        #     cursor.execute("UPDATE cnc SET threshold_10 = " + str(threshold_10)+ " WHERE word_id = "+ str(cnc_id))
        #     connection.commit()

        # # threshold average
        # #print (cnc_threshold_values_list)
        # print('10% threshold average:') 
        # print(general_utils.get_list_average (cnc_threshold_values_list))

# 15%

        # #cnc_threshold_dict = {}
        # cnc_threshold_values_list = [] # just to have only values printed

        # # adding new column - with % threshold
        # cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_15 float(32) NOT NULL") 

        # for cnc_id in cnc_list:
        #     #print(cnc_id)
        #     threshold_15 = get_cnc_threshold_by_percent(cnc_id, 0.15, 1, cursor)
        #     #cnc_threshold_dict.update({cnc_id: threshold_15}) # dict 'cnc:threshold_value'
        #     cnc_threshold_values_list.append(threshold_15)
            
        #     cursor.execute("UPDATE cnc SET threshold_15 = " + str(threshold_15)+ " WHERE word_id = "+ str(cnc_id))
        #     connection.commit()

        # # threshold average
        # #print (cnc_threshold_values_list)
        # print('15% threshold average:') 
        # print(general_utils.get_list_average (cnc_threshold_values_list))

# 20%

        # #cnc_threshold_dict = {}
        # cnc_threshold_values_list = [] # just to have only values printed

        # # adding new column - with % threshold
        # cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_20 float(32) NOT NULL") 

        # for cnc_id in cnc_list:
        #     #print(cnc_id)
        #     threshold_20 = get_cnc_threshold_by_percent(cnc_id, 0.2, 1, cursor)
        #     #cnc_threshold_dict.update({cnc_id: threshold_20}) # dict 'cnc:threshold_value'
        #     cnc_threshold_values_list.append(threshold_20)
            
        #     cursor.execute("UPDATE cnc SET threshold_20 = " + str(threshold_20)+ " WHERE word_id = "+ str(cnc_id))
        #     connection.commit()

        # # threshold average
        # #print (cnc_threshold_values_list)
        # print('20% threshold average:') 
        # print(general_utils.get_list_average (cnc_threshold_values_list))


# 25%

        # #cnc_threshold_dict = {}
        # cnc_threshold_values_list = [] # just to have only values printed

        # # adding new column - with % threshold
        # cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_25 float(32) NOT NULL") 

        # for cnc_id in cnc_list:
        #     #print(cnc_id)
        #     threshold_25 = get_cnc_threshold_by_percent(cnc_id, 0.25, 1, cursor)
        #     #cnc_threshold_dict.update({cnc_id: threshold_25}) # dict 'cnc:threshold_value'
        #     cnc_threshold_values_list.append(threshold_25)
            
        #     cursor.execute("UPDATE cnc SET threshold_25 = " + str(threshold_25)+ " WHERE word_id = "+ str(cnc_id))
        #     connection.commit()

        # # threshold average
        # #print (cnc_threshold_values_list)
        # print('25% threshold average:') 
        # print(general_utils.get_list_average (cnc_threshold_values_list))


# 30%

        # #cnc_threshold_dict = {}
        # cnc_threshold_values_list = [] # just to have only values printed

        # # adding new column - with % threshold
        # cursor.execute("ALTER TABLE cnc ADD COLUMN threshold_30 float(32) NOT NULL") 

        # for cnc_id in cnc_list:
        #     #print(cnc_id)
        #     threshold_30 = get_cnc_threshold_by_percent(cnc_id, 0.3, 1, cursor)
        #     #cnc_threshold_dict.update({cnc_id: threshold_25}) # dict 'cnc:threshold_value'
        #     cnc_threshold_values_list.append(threshold_30)
            
        #     cursor.execute("UPDATE cnc SET threshold_30 = " + str(threshold_30)+ " WHERE word_id = "+ str(cnc_id))
        #     connection.commit()

        # # threshold average
        # #print (cnc_threshold_values_list)
        # print('30% threshold average:') 
        # print(general_utils.get_list_average (cnc_threshold_values_list))



