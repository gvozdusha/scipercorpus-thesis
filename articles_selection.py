# this script extracts 3 lists of articles (by research field: biology, linguistics, economics)
# and randomly selects equal number of articles in each field (54)
# then a new column is added to the word table with just selected articles marked "true"

# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

# For pseudo-randon selections
import random
import sys


def get_all_articles_in_field (field, cursor):
    articles_list = [] # list of titles

    cursor.execute("SELECT DISTINCT file FROM word WHERE subcorpus = '"+field+"'")
    rows = cursor.fetchall()

    for i in range(len(rows)):
        articles_list.append(rows[i][0])

    return articles_list

def get_selection_of_articles_in_field (articles_list, amount_per_field):
    #seed = random.randint (-sys.maxsize-1, sys.maxsize)
    seed = -1870565726765940253   #fixing the value  
    random.seed (seed) 
    
    return random.sample(articles_list, amount_per_field)


if __name__ == '__main__':

    amount_per_field = 54

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()
        bio = get_all_articles_in_field ("biology", cursor)
        ling = get_all_articles_in_field ("linguistics", cursor)
        econ = get_all_articles_in_field ("economics", cursor)

    bio_select = get_selection_of_articles_in_field (bio, amount_per_field)
    ling_select = get_selection_of_articles_in_field (ling, amount_per_field)
    econ_select = get_selection_of_articles_in_field (econ, amount_per_field)

    all_selected_articles = bio_select + ling_select + econ_select

    #print(len(all_selected_articles))

    ##########################
    
    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

#add a new column to the table - boolean with False as a default (TinyInt 0)    
        #cursor.execute("ALTER TABLE word ADD COLUMN file_selected BOOLEAN DEFAULT FALSE NOT NULL") 

#set flag TRUE for selected articles
        # for i in all_selected_articles:
        #     cursor.execute("UPDATE word SET file_selected = TRUE WHERE file = '"+ i +"'")
        #     connection.commit()

