# this script gets the list of all CNC-s in selected articles
# then a round of filtration takes place in order to clean "bad" CNC-s based on regex
# and a new DB table with the discriptive info about these CNC-s is created
# there is also the info about "enough length" of the CNC's samples



# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

# module for CNC processing
import cnc_tools

# module for google sheets generation
from google_spreadsheet_utils import get_gsheet_for_annotation 

# regex
import re


if __name__ == '__main__':

    ncList = [] # start indexes of all nominal compounds (2+ nouns) in the selected articles    
    cncList = [] # start indexes of all complex nominal compounds (3+ nouns) in the selected articles

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        #########################
        # getting all the nominal compounds (2+) in the selected articles and adding them to the array ncList
        # selection cuts NNP-type that shouldn't be as a part of CNC
        
        cursor.execute("SELECT id FROM word WHERE bilou = 'B' AND part_of_speech NOT LIKE 'NNP%' AND file_selected is TRUE")  

        rows = cursor.fetchall()

        for i in range(len(rows)):
            ncList.append(int(rows[i][0]))

        #########################
        #filtering 3+ length nominal compounds that are called Complex nominal compounds
        #that means that after lable 'B' there must be 'I' (2nd noun in row)

        for i in ncList:
            cursor.execute("SELECT id FROM word WHERE bilou = 'I' AND part_of_speech NOT LIKE 'NNP%' AND id = "+str(i+1))
            row = cursor.fetchone()
            if row != None:
                cncList.append(int(i)) # store Start (lable 'B') index of CNC
                #print (i)

        # Second way of CNC selection using get_cnc_length func BUT it works much slower
        # for i in ncList:
        #     if cnc_tools.get_cnc_length(i,cursor)>2:
        #         cncList.append(i)  


        #########################
        #creating a new table in DB

        # cursor.execute("CREATE TABLE `cnc` ("+
        #   "`id` int(11) unsigned NOT NULL AUTO_INCREMENT,"+
        #   "`word_id` int(10) unsigned NOT NULL,"+
        #   "`cnc_length` int(10) NOT NULL,"+
        #   "`cnc_string` varchar(255) NOT NULL,"+
        #   "PRIMARY KEY (`id`),"+
        #   "KEY `word_id` (`word_id`),"+
        #   "CONSTRAINT `word_vectors_ibfk_2` FOREIGN KEY (`word_id`) REFERENCES `word` (`id`)"+
        # ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;")


        ############################
        #applying filtration (and filling in CNC table at the same time if needed)
        
        filtered_cncList = [] 

        for i in cncList:
            cnc_string = cnc_tools.get_cnc_string(i, cursor)
            if not re.search('( .{1} )|(^.{1} )|([^a-zA-Z -])|([a-zA-Z]+- )', cnc_string):
                # cursor.execute("INSERT INTO cnc (word_id, cnc_length, cnc_string) VALUES ("+str(i)+", "+str(cnc_tools.get_cnc_length(i, cursor))+", '"+cnc_string+"')")
                # connection.commit()
                filtered_cncList.append(i)
            else: 
                print (cnc_string)


        #print (len(filtered_cncList)) #5234 CNCs


        #########################
        # adding a new column that indicates that the sample of the preceding text is long enough (doesn't cross a file border)
        # and so this CNC should be considered for further analysis

        #cursor.execute("ALTER TABLE cnc ADD COLUMN solid_sample BOOLEAN DEFAULT FALSE NOT NULL") 

        #solid_sample_flags =[]
        # for i in filtered_cncList:
        #     print (i)
        #     if (cnc_tools.get_sample_start_index(i, cursor) != -1):
        #         cursor.execute("UPDATE cnc SET solid_sample = TRUE WHERE word_id = '"+ str(i) +"'")
        #         connection.commit()
            

            #     solid_sample_flags.append(1)
            # else: solid_sample_flags.append (0)

