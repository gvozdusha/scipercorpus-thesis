import httplib2 
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials

import consts

#for the dalay when Google API is used
import time


def gsheet_creator (file_name, service, httpAuth): # creates an empty google spreadsheet
    
    spreadsheet = service.spreadsheets().create(body = {
        'properties': {'title': file_name},      # by default locale is 'en_UK'
        'sheets': [{'properties': {'sheetType': 'GRID',
                                   'sheetId': 0,
                                   'title': 'List1',
                                   'gridProperties': {'rowCount': consts.JUMP_LENGTH+100, 'columnCount': 12}
                                   }}]
    }).execute()
    spreadsheetId = spreadsheet['spreadsheetId'] # save file id
    #print('https://docs.google.com/spreadsheets/d/' + spreadsheetId)

    #permisions for the file
    driveService = apiclient.discovery.build('drive', 'v3', http = httpAuth) # Google Drive API v3
    access = driveService.permissions().create(
        fileId = spreadsheetId,
        body = {'type': 'user', 'role': 'writer', 'emailAddress': 'scipercorpus@gmail.com'},  # permisions as a an edditor
        fields = 'id'
    ).execute()
    return spreadsheetId

def gsheet_format (spreadsheetId, service):

    results = service.spreadsheets().batchUpdate(spreadsheetId = spreadsheetId, body = {
      "requests": [

    #froze the first 2 rows as a header
        {
          "updateSheetProperties": {
            "properties": {
              "sheetId": 0,
              "gridProperties": {
                "frozenRowCount": 2
              }
            },
            "fields": "gridProperties.frozenRowCount"
          }
        },

    # cells' format: grey words' indexes
        { # 1st line coloring to grey
        "repeatCell": 
          {
            "cell": 
            {
            "userEnteredFormat": 
            {
                "textFormat":
                {
                    "foregroundColor": {
                        "red": 0.811,
                        "green": 0.811,
                        "blue": 0.811,
                        "alpha": 1
                    }
                }
            }
            },
            "range": 
            {
              "sheetId": 0,
              "startRowIndex": 0,
              "endRowIndex": 1,
              "startColumnIndex": 0,
              "endColumnIndex": 12
            },
            "fields": "userEnteredFormat"
          }
        } ,

        { # 1st column coloring to grey
        "repeatCell": 
          {
            "cell": 
            {
            "userEnteredFormat": 
            {
                "textFormat":
                {
                    "foregroundColor": {
                        "red": 0.811,
                        "green": 0.811,
                        "blue": 0.811,
                        "alpha": 1
                    }
                }
            }
            },
            "range": 
            {
              "sheetId": 0,
              "startRowIndex": 0,
              "endRowIndex": consts.JUMP_LENGTH+50,
              "startColumnIndex": 0,
              "endColumnIndex": 1
            },
            "fields": "userEnteredFormat"
          }
        } ,

        ## setting the wheight of colunm B with sample words
        {
          "updateDimensionProperties": {
            "range": {
              "sheetId": 0,
              "dimension": "COLUMNS",
              "startIndex": 1,
              "endIndex": 2
            },
            "properties": {
              "pixelSize": 200
            },
            "fields": "pixelSize"
          }
        },

        #  bold cnc and sample words + colunm B is set with right alignment

        {
          "repeatCell": 
          {
            "cell": 
            {
              "userEnteredFormat": 
              {
                "horizontalAlignment": 'Right',
                "textFormat":
                 {
                   "bold": True,
                   "fontSize": 12
                 }
              }
            },

            "range": 
            {
              "sheetId": 0,
              "startRowIndex": 2,
              "endRowIndex": consts.JUMP_LENGTH+50,
              "startColumnIndex": 1,
              "endColumnIndex": 2
            },
            "fields": "userEnteredFormat"
          }
        },
        {
          "repeatCell": 
          {
            "cell": 
            {
              "userEnteredFormat": 
              {
                "textFormat":
                 {
                   "bold": True,
                   "fontSize": 12
                 }
              }
            },

            "range": 
            {
              "sheetId": 0,
              "startRowIndex": 1,
              "endRowIndex": 2,
              "startColumnIndex": 0,
              "endColumnIndex": 12
            },
            "fields": "userEnteredFormat"
          }
        },

      ]
    }).execute()

    return 0

# func gets 2 dictionaries with the data (cnc and sample) and adds this data to the google spreadsheet
def gsheet_filling (cnc, sample, spreadsheetId, service): 

    results = service.spreadsheets().values().batchUpdate(spreadsheetId = spreadsheetId, body = {
    "valueInputOption": "USER_ENTERED",
    "data": [
        {"range": "List1!C1:2",
         "majorDimension": "COLUMNS",   
         "values": cnc},
         {"range": "List1!A3:B",
         "majorDimension": "ROWS", 
         "values": sample},
    ]
    }).execute()

    return 0

# this function aggregates 3 other functions that create, format and fill in a google spreadsheet
# in order to produce a file for the manual annotation of preceding words of the given cnc
def get_gsheet_for_annotation(cnc, sample, file_name):

    CREDENTIALS_FILE = '../cnc-annotation-files-f1b82b66a31a.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'])
    httpAuth = credentials.authorize(httplib2.Http()) # auth in the system
    service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)

    # creating an empty google spreadsheet and saving its ID
    spreadsheetId = gsheet_creator(file_name, service, httpAuth)

    # applying formats for the existing google spreadsheet
    gsheet_format (spreadsheetId, service)

    # adding the data - cnc and its text sample
    gsheet_filling (cnc, sample, spreadsheetId, service)

    # print the google spreadsheet link
    print('https://docs.google.com/spreadsheets/d/' + spreadsheetId)

    return 0

###################
#these tools extract the data from gsheets
#1) complete set of marked words per cnc
#2) sets of marked words per cnc components

# link -> dictionary  [cnc id, [set of indexes of preceding words from a sample]]
def get_annotated_cnc_preceedings_set_from_gsheet (spreadsheetId):

    CREDENTIALS_FILE = '../cnc-annotation-files-f1b82b66a31a.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive'])
    httpAuth = credentials.authorize(httplib2.Http()) # auth in the system
    service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)

    #getting CNC index which is stored in C1 cell
    values = service.spreadsheets().values().get(

      spreadsheetId = spreadsheetId,
      range = 'C1',
      majorDimension = 'ROWS'

      ).execute()

    cnc_index = int(values["values"][0][0])

    values = service.spreadsheets().values().get(

      spreadsheetId = spreadsheetId,
      range = 'A3:L300',
      majorDimension = 'ROWS'

      ).execute()

    content = values["values"]
    preceedings_set = set()

    #srearching for rows with any annotation marks
    for c in content:
      if len(c)>2:  # not just index and word in a row, but some marks as well
        preceedings_set.add(int(c[0]))

    time.sleep(7)
    return {cnc_index: preceedings_set} # data structure - dictionary [cnc_index: set of indexes]




