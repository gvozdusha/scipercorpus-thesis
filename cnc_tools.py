import consts
import random

#mkdir
import os

def get_cnc_length (cnc_index, cursor):
    
    res = 0     #flag indicates that it is a 'BI*L' segment

    cursor.execute("SELECT bilou FROM word WHERE id = "+str(cnc_index))
    row = cursor.fetchone()
    bilou_value = row[0]
    
    if bilou_value != 'B':      # given start (cnc_index) is not the beggining of cnc
        res = 1                 # set flag
    
    temp_index = cnc_index
    while bilou_value != 'L':
        cursor.execute("SELECT bilou FROM word WHERE id = "+str(temp_index+1))
        row = cursor.fetchone()
        bilou_value = row[0]
        temp_index+=1

        if (row[0] != 'I') and (row[0] !='L'): # then it is O/U/..
            res = 1

    if res == 0:
        cnc_length = temp_index-cnc_index+1 # calculate the length from 'B' to 'L'
    else: 
        cnc_length = -1

    return cnc_length # it may be equal 2  => it is a NC, but not a CNC

def get_cnc_dict(cnc_index, cursor):
# the func represents a CNC as a dictionary like [[151, 'spot'], [152, 'market'], [153, 'power']
    cnc_dict = []

    cnc_length = get_cnc_length(cnc_index, cursor)

    if cnc_length != -1:
        for i in range(cnc_length):
            cursor.execute("SELECT word FROM word WHERE id = "+str(cnc_index+i))
            row = cursor.fetchone()
            word = row[0]
            cnc_dict.append([cnc_index+i, word])
    else: 
        print ('WARNING: empty cnc dictionary; cnc_index: '+ str(cnc_index))

    return cnc_dict

def get_sample_start_index (cnc_index, cursor):
# this func finds the beggining of a CNC text sample
# the sample has to have a complete sentence at the beggining (the search starts 100 tockens earlier then)


# checking that cnc_index correcpondes to a CNC

    if (get_cnc_length(cnc_index, cursor)<3): 
        print ('WARNING: sample_start_index is requested for not CNC; cnc_index: '+ str(cnc_index))
        return -1

# processing corner cases with the beggining (<JUMP_LENGTH) of the corpus
 
    if (cnc_index < consts.JUMP_LENGTH):
        print ('WARNING: sample requested for CNC is too short; cnc_index: '+ str(cnc_index))
        return -1

# search
        
    temp_index = cnc_index - consts.JUMP_LENGTH - 100 # the search start point
        
    if (temp_index <  0): # a case when the sample size is potentially enough but the search start point is behind id=0
        temp_index = 0    # reassinging the beggining of search to the first element in the corpus


    #print ('JUMP'+str(cnc_index - consts.JUMP_LENGTH))
    #print ('temp'+str(temp_index))

    sample_start_index = 0 # variable initialization

    while temp_index < cnc_index - consts.JUMP_LENGTH:
        cursor.execute("SELECT word_in_sentence FROM word WHERE id = "+str(temp_index))
        row = cursor.fetchone()
        word_in_sentence = row[0]
        #print (row[0])

        if word_in_sentence == 0:
            sample_start_index = temp_index # save the current candidate for the start position (but for the nearest - loop)

        temp_index+=1

    # check that there is no file border in the founded sample:
    for i in range(sample_start_index, cnc_index):
        cursor.execute("SELECT word_in_file FROM word WHERE id = "+str(i))
        row = cursor.fetchone()
        word_in_file = row[0]
        if word_in_file == 0:
            print ('WARNING: sample requested for CNC crosses a file border; cnc_index: '+ str(cnc_index))
            return -1


    return sample_start_index



def get_sample_dict(cnc_index, cursor):
# the func represents a sample as a dictionary like [[31, 'I'], [32, 'am'], [33, 'going'], [34, 'to'], ....]

    sample_dict = []

    temp_index = get_sample_start_index(cnc_index, cursor)

    if temp_index != -1:
        while temp_index != cnc_index :
            cursor.execute("SELECT word FROM word WHERE id = "+str(temp_index))
            row = cursor.fetchone()
            word = row[0]
            sample_dict.append([temp_index, word])
            temp_index+=1

    else: 
        print ('WARNING: empty sample dictionary; cnc_index: '+ str(cnc_index))

    return sample_dict

def get_cnc_string(cnc_index, cursor):

    cnc_length = get_cnc_length(cnc_index, cursor)
    cnc_string =''

    for i in range(cnc_length):
        cursor.execute("SELECT word FROM word WHERE id = "+str(cnc_index+i))
        row = cursor.fetchone()
        cnc_string = cnc_string+row[0]+' '

    return cnc_string

def get_all_cnc_in_field (field, cursor):
#this func returns a list of all cncs that are contaned in CNC-table in DB and belong to a given field/subcorpus
    cnc_list = [] # list of cnc in one field

    cursor.execute("SELECT cnc.word_id FROM word join cnc ON word.id = cnc.word_id and subcorpus ="+"'"+field+"'")
    rows = cursor.fetchall()

    for i in range(len(rows)):
        cnc_list.append(rows[i][0])

    return cnc_list

def get_full_sample_end_index (cnc_index, cursor):
#calculates the end of the sentence after the cnc to extract the full sentence for reading
    
    # search start initialization: index and word_in_sentence value
    temp_index = cnc_index + get_cnc_length(cnc_index, cursor) 
    
    cursor.execute("SELECT word_in_sentence FROM word WHERE id = "+str(temp_index))
    row = cursor.fetchone()
    word_in_sentence = row[0]

    # searching the beggining of the next sentence 
    while word_in_sentence!=0:
        temp_index+=1
        cursor.execute("SELECT word_in_sentence FROM word WHERE id = "+str(temp_index))
        row = cursor.fetchone()
        word_in_sentence = row[0]
        #print (row[0])
        
    return temp_index-1


def get_full_sample_as_text (cnc_index, cursor):

    text = ""
    sample_start_index = get_sample_start_index (cnc_index, cursor)
    sample_end_index = get_full_sample_end_index (cnc_index, cursor)


    if sample_start_index!= 0:
        for i in range (sample_end_index - sample_start_index + 1):
            cursor.execute("SELECT word FROM word WHERE id = "+str(sample_start_index+i))
            row = cursor.fetchone()
            text = text+row[0]+' '

    return text
# TODO improve func that retrieves a "full sample" as a text/string: delete spaces before punctuation marks.


# creates a dict of "sample_word_id-s, cossim values" for the given comp_word_id 
#like [[37208, 0.28880602], [37209, 0.35399517], ...] for 37409
def get_cnc_component_cossim_dict (comp_word_id, cursor):
    comp_cossim_dict = []
    temp_dict = []

    cursor.execute ("SELECT sample_word_id, cossim FROM similarities WHERE comp_word_id = "+str(comp_word_id)) # can be added when needed: AND WHERE it is not a functional word (by flag in DB)
    rows = cursor.fetchall()

    for i in range(len(rows)):
        comp_cossim_dict.append([int(rows[i][0]), float(rows[i][1])])

    return comp_cossim_dict

def get_all_cnc (cursor):
#this func returns a list of all cncs that are contaned in CNC-table AND 'relevant for similarity' (solid_sample is true)
    cnc_list = []

    cursor.execute("SELECT cnc.word_id FROM cnc WHERE solid_sample is true")
    rows = cursor.fetchall()

    for i in range(len(rows)):
        cnc_list.append(rows[i][0])

    return cnc_list


#############################
# Tools for getting BERT sets


# selecting the list of sample_word_id-s (and not func/irrel words) that are considered to be precedings 
#for the "cnc component" by applying a numeric threshold
def get_cnc_component_preceedings_list_by_threshold(comp_word_id, threshold, cursor):
    comp_preceedings_list = []

    comp_cossim_dict = get_cnc_component_cossim_dict(comp_word_id, cursor)

    for c in comp_cossim_dict:
        if c[1] >= threshold:
            #cursor.execute("SELECT sample_word_is_func FROM similarities WHERE sample_word_id = " + str(c[0]) + " AND comp_word_id = " + str(comp_word_id))
            cursor.execute("SELECT sample_word_is_irrel FROM similarities WHERE sample_word_id = " + str(c[0]) + " AND comp_word_id = " + str(comp_word_id))
            row = cursor.fetchone()
            if row[0] == 0: # not func/irrel word
                comp_preceedings_list.append(c[0])

    #print (len(comp_precedings_list))
    return comp_preceedings_list

# set (as union of the components' sets) of preceding words of the cnc
# is calculated based on the certain given numeric threshold 
def get_cnc_preceedings_set_by_threshold (cnc_word_id, threshold, cursor):
    cnc_preceedings_list = []

    cnc = get_cnc_dict(cnc_word_id, cursor)

    for i in cnc:
        comp_list = get_cnc_component_preceedings_list_by_threshold(i[0], threshold, cursor)
        cnc_preceedings_list += comp_list

    #print (len(cnc_precedings_list))
    return set(cnc_preceedings_list)

# set of preceding words selected randomly

################ random sets
def get_cnc_preceedings_list_by_rand(comp_word_id, cursor):

    comp_preceedings_list = []

    comp_cossim_dict = get_cnc_component_cossim_dict(comp_word_id, cursor)

    for c in comp_cossim_dict:           
        cursor.execute("SELECT sample_word_is_irrel FROM similarities WHERE sample_word_id = " + str(c[0]) + " AND comp_word_id = " + str(comp_word_id))
        row = cursor.fetchone()
        if row[0] == 0: # not func/irrel word
            if (random.uniform(0,1) <= 0.25):
                #print(random.uniform(0,1))
                comp_preceedings_list.append(c[0])

    #return comp_preceedings_list
    return set(comp_preceedings_list)


def create_cnc_txt_file (cnc_word_id, cursor):

    # get parametrs
    cursor.execute("SELECT cnc_length, preceeding_words, cnc_string FROM cnc WHERE word_id = " + str(cnc_word_id))
    row = cursor.fetchone()

    cnc_length = row[0]
    preceeding_words = row[1]
    cnc_string = row[2]

    cursor.execute("SELECT subcorpus FROM word WHERE id = " + str(cnc_word_id))
    row = cursor.fetchone()
    cnc_subcorpus = row[0]
    
    # prepare attr-s for file-name and file-content

    attr_cnc_length = 5 if cnc_length > 4 else cnc_length

    if (preceeding_words >9) and (preceeding_words <20): # 10-19 A
        attr_preceeding_words = 'A'
    elif (preceeding_words >19) and (preceeding_words <30): # 20-29 B
        attr_preceeding_words = 'B'
    elif (preceeding_words >29) and (preceeding_words <40): # 30-39 C
        attr_preceeding_words = 'C'
    elif (preceeding_words >39): # 39+ C+/D
        attr_preceeding_words = 'D'

    attr_subcorpus = cnc_subcorpus[0:3]


    # create and fill-in the file
    path = "../cnc_collection/" +   str(attr_cnc_length)+"_"+attr_preceeding_words+"_"+attr_subcorpus    +"/"
    full_path =  path   +"/" +str(attr_cnc_length)+"_"+attr_preceeding_words+"_"+attr_subcorpus+"_"+str(cnc_word_id) + ".txt"
    #f = open(path,"w")
    try: 
        os.mkdir(path) 
    except OSError as error:
        0

    f = open(full_path,"w")
    f.write(cnc_string+"\n\n")
    f.write(get_full_sample_as_text(cnc_word_id, cursor))

    #printing file name
    print(str(attr_cnc_length)+"_"+attr_preceeding_words+"_"+attr_subcorpus+"_"+str(cnc_word_id))

    return 0

# func calculates the group/cell to which a cnc belongs based on its length, number of preceding words and subcorpus field
def calc_cnc_group_key(cnc_word_id, cursor):
    # get parametrs
    cursor.execute("SELECT cnc_length, preceeding_words, cnc_string FROM cnc WHERE word_id = " + str(cnc_word_id))
    row = cursor.fetchone()

    cnc_length = row[0]
    preceeding_words = row[1]
    cnc_string = row[2]

    cursor.execute("SELECT subcorpus FROM word WHERE id = " + str(cnc_word_id))
    row = cursor.fetchone()
    cnc_subcorpus = row[0]
    
    # prepare attr-s for file-name and file-content

    attr_cnc_length = 5 if cnc_length > 4 else cnc_length

    if (preceeding_words >9) and (preceeding_words <20): # 10-19 A
        attr_preceeding_words = 'A'
    elif (preceeding_words >19) and (preceeding_words <30): # 20-29 B
        attr_preceeding_words = 'B'
    elif (preceeding_words >29) and (preceeding_words <40): # 30-39 C
        attr_preceeding_words = 'C'
    elif (preceeding_words >39): # 39+ C+/D
        attr_preceeding_words = 'D'

    attr_subcorpus = cnc_subcorpus[0:3]

    return str(attr_cnc_length)+"_"+attr_preceeding_words+"_"+attr_subcorpus

    

