# creating files for the manual annotation

# this script selects around 20 cnc-s per subject
# selection is random and takes place on the cnc-table
# then google sheets are generated for each selected cnc

# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

# module for CNC processing
import cnc_tools

import general_utils

# module for google sheets tools (generation, filling, extraction)
from google_spreadsheet_utils import get_gsheet_for_annotation 

# For pseudo-randon selections
import random
import sys


def get_gsheets_for_field (field, gsheets_per_field, cursor):

    all_cnc_in_field = cnc_tools.get_all_cnc_in_field (field, cursor)
    selected_cnc_in_field = general_utils.get_random_items_from_list (all_cnc_in_field, gsheets_per_field)
    #print (selected_cnc_in_field)

    for i in selected_cnc_in_field: # for each selected CNC get a gsheet link
        cnc_dict = cnc_tools.get_cnc_dict (i, cursor)
        sample_dict = cnc_tools.get_sample_dict (i, cursor)
        get_gsheet_for_annotation (cnc_dict, sample_dict, field+'_'+str(i))

    return 0


if __name__ == '__main__':

    gsheets_per_field = 20 

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        get_gsheets_for_field ("biology", gsheets_per_field, cursor)
        get_gsheets_for_field ("linguistics", gsheets_per_field, cursor)
        get_gsheets_for_field ("economics", gsheets_per_field, cursor)


