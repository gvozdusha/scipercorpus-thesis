# this script calculates avarage percent of words (only relevant) that are annotated manually
# such percentage is used as one more candidate to calculate "threshold by percent"


# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

# module for google sheets tools (generation, filling, extraction)
from google_spreadsheet_utils import get_annotated_cnc_preceedings_set_from_gsheet

import general_utils


if __name__ == '__main__':

    percent_list = [] # contains percents per manually annotated cnc

    gsheets_id_file = "../gsheets_IDs.txt" # file with all spreadsheet IDs
    gsheets_id_list = general_utils.get_list_from_txt(gsheets_id_file)


    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        for spreadsheetID in gsheets_id_list:
            #print (spreadsheetID)
            d = get_annotated_cnc_preceedings_set_from_gsheet(spreadsheetID)
            x = len(list(d.values())[0]) # annotated sample_words length

            # sample length with no irrel words is extracted
            cursor.execute("SELECT COUNT(sample_word_id) FROM similarities WHERE sample_word_is_irrel is false and comp_word_id =" + str(list(d.keys())[0]) ) # comp_word_id is used instead of cnc_word_id because only one sample copy is needed
            row = cursor.fetchone()
            y = row[0]
            #print (y)

            percent_list.append(general_utils.get_percentage_x_of_y(y, x))

    # calculating the "human percentage candidate"    
    print (percent_list)
    
    print ("Avarage percent is  " + str(general_utils.get_list_average(percent_list)))

