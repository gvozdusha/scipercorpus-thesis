# this script uses a file with the list of function english words (1)
# and marks such words in the DB in the similarities-table (new boolean column)
# as well as "words" that are numbers, punctuation marks or contain numbers  (2)
# both categories -> irrelevant sample words

# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

import general_utils

# regex
import re


def detect_func_word (sample_word_id, func_words_list, cursor):
    flag = 0

    cursor.execute("SELECT word FROM word WHERE id=" + str(sample_word_id))
    row = cursor.fetchone()

    if row[0].lower() in func_words_list: #lowercased
        flag = 1

    return flag

def detect_non_word (sample_word_id, cursor):
    flag = 0

    cursor.execute("SELECT word FROM word WHERE id=" + str(sample_word_id))
    row = cursor.fetchone()

    if row[0].lower() == "l1" or row[0].lower() == "l2":
        return flag
    if (not re.search('[a-zA-Z]+', row[0])) or (re.search('[0-9]+', row[0])):
        flag = 1
        #print (row[0])

    return flag




if __name__ == '__main__':

    func_words_file = "../func_words.txt" # the list was created based on https://semanticsimilarity.wordpress.com/function-word-lists/
    func_words_list = general_utils.get_list_from_txt(func_words_file)

    #print (func_words_list)

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        # adding new column - sample_word_is_func
        cursor.execute("ALTER TABLE similarities ADD COLUMN sample_word_is_func BOOLEAN DEFAULT FALSE NOT NULL") 
        cursor.execute("ALTER TABLE similarities ADD COLUMN sample_word_is_irrel BOOLEAN DEFAULT FALSE NOT NULL") 


        cursor.execute("SELECT sample_word_id FROM similarities") # WHERE cnc_word_id > 677600
        rows = cursor.fetchall()

        for i in range(len(rows)):
            #print (rows[i][0])
            print (i)
            func_flag = detect_func_word(rows[i][0], func_words_list, cursor)
            if func_flag:
                cursor.execute("UPDATE similarities SET sample_word_is_func = TRUE WHERE sample_word_id = "+ str(rows[i][0]))            

            if func_flag or detect_non_word(rows[i][0], cursor):
                cursor.execute("UPDATE similarities SET sample_word_is_irrel = TRUE WHERE sample_word_id = "+ str(rows[i][0]))
            
            connection.commit()

