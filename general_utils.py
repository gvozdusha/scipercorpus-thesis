# For pseudo-randon selections
import random
import sys

def get_random_items_from_list (list_of_items, amount):
    #seed = random.randint (-sys.maxsize-1, sys.maxsize)
    #print (seed)
    seed = -8746060140659434574   #fixing the value  
    random.seed (seed) 
    
    return random.sample(list_of_items, amount)

def get_list_from_txt (file_name):
    list_from_txt = []

    f = open(file_name, "r")
    lines = f.readlines()
    for l in lines:
        list_from_txt.append(l.strip())

    return list_from_txt

def get_list_average (given_list):

	sum = 0
	for l in given_list:
		sum+=l

	return sum/len(given_list)

def get_percentage_x_of_y (total, part):

	return part*100/total

def get_extended_by_zeros_list (start, finish, l):
# to get the annotation like yes/no
	idx = 0
	res = []

	for i in range(start, finish):
		if (idx<len(l)) and (i >= l[idx]):
			#res.append(l[idx]) # to keep selected indexes
			res.append(1) # to make it binary: yes/no - 2 cagegories
			idx+=1
		else:
			res.append(0)

	return res
