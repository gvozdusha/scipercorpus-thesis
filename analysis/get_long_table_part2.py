import csv

def read_abbr(num):
  name = "part2_lists/q%02d.txt" % num
  fo = open(name, "r")
  ss = fo.readlines()
  fo.close()
  ss = list(map(lambda x: x.rstrip(), ss))

  assert len(ss) == 20
  return ss

abbr_table = []

to_exclude_file = open("toexcludeL1.txt", "r") # the file to be changed
to_exclude = to_exclude_file.readlines()
to_exclude_file.close()
to_exclude = list(map(lambda x: x.rstrip(), to_exclude))

for i in range(12):
  ss = read_abbr(i)
  abbr_table.append(ss)

print("To exclude:", to_exclude)

with open('all_forms_L1.csv', newline='') as csvfile, open('out_L1_part2.csv', 'w') as csvwfile:  #the files to be changed
  spamreader = csv.reader(csvfile)
  csvwriter = csv.writer(csvwfile)
  header = ['form',	'participant', 'q', 'cnc_len', 'sim_words', 'field', 'item']
  csvwriter.writerow(header)
  rows_count = 0
  for row in spamreader:
    user = row[0]
    if user in to_exclude:
      continue
    q = int(user[1:3])
    print(user)

    rows_count += 1
    for i in range(20):
      w = []
      w.append('Q%02d' % q)
      w.append(user)
      w.append(row[56 + i])
      abbr = abbr_table[q][i]

      w.append(abbr[0:1])
      w.append(abbr[2:3])
      w.append(abbr[4:7])
      w.append(abbr)

      csvwriter.writerow(w)

  print("Processed: ", rows_count)

