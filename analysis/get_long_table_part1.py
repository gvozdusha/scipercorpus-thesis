import csv

def read_abbr(num):
  name = "part1_lists/q%02d.txt" % num
  fo = open(name, "r")
  ss = fo.readlines()
  fo.close()
  ss = list(map(lambda x: x.rstrip(), ss))
  j = 1
  for i, x in enumerate(ss):
    ss[i] = "C%d" % j if x == 'Catch' else x
    if x == 'Catch':
      j += 1 
  del ss[:5]
  assert len(ss) == 23
  return ss

abbr_table = []

to_exclude_file = open("toexcludeL1.txt", "r")  #the file to be changed
to_exclude = to_exclude_file.readlines()
to_exclude_file.close()
to_exclude = list(map(lambda x: x.rstrip(), to_exclude))

for i in range(12):
  ss = read_abbr(i)
  abbr_table.append(ss)

print("To exclude:", to_exclude)

with open('all_forms_L1.csv', newline='') as csvfile, open('out_L1_part1.csv', 'w') as csvwfile, open('out_catch_L1_part1.csv', 'w') as csvcfile:  #the files to be changed
  spamreader = csv.reader(csvfile)
  csvwriter = csv.writer(csvwfile)
  cwriter = csv.writer(csvcfile)
  header = ['form',	'participant', 'q1', 'q2', 'cnc_len', 'sim_words', 'field', 'item']
  csvwriter.writerow(header)
  cwriter.writerow(header)
  rows_count = 0
  for row in spamreader:
    user = row[0]
    if user in to_exclude:
      continue
    q = int(user[1:3])
    print(user)

    rows_count += 1
    for i in range(23):
      w = []
      w.append('Q%02d' % q)
      w.append(user)
      w.append(row[9 + i*2])
      w.append(row[9 + i*2 + 1])
      abbr = abbr_table[q][i]
      if abbr == 'C1' or abbr == 'C2' or abbr == 'C3':
        w.append(0)
        w.append(0)
        w.append(0)
      else:
        w.append(abbr[0:1])
        w.append(abbr[2:3])
        w.append(abbr[4:7])
      w.append(abbr)
      if abbr == 'C1' or abbr == 'C2' or abbr == 'C3':
        cwriter.writerow(w)
      else:
        csvwriter.writerow(w)

  print("Processed: ", rows_count)

