# this script adds a column to the cnc-table that indicates repetitions of the cnc itself in its sample
# it is needed to exclude such cnc-s from the collection

# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

# module for CNC processing
import cnc_tools

# regex
import re


def check_cnc_occurence_in_sample (cnc, cursor):
    flag = -1
    sample = cnc_tools.get_full_sample_as_text(cnc, cursor)
    cnc_string = cnc_tools.get_cnc_string(cnc, cursor)

    if sample.count(cnc_string)>1:
        flag = 1
    elif sample.count(cnc_string)==1:
        flag = 0

    return flag


if __name__ == '__main__':

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        # add a new column (NULL - not aaaplied, "sample" cannot be exctacted; 1 - cnc in sample; 0 - cnc is not repeated)
        #cursor.execute("ALTER TABLE cnc ADD COLUMN cnc_in_sample BOOLEAN DEFAULT NULL") 



        # get all cnc in cnc-table
        cnc_list = []

        cursor.execute("SELECT cnc.word_id FROM cnc WHERE solid_sample is true")
        rows = cursor.fetchall()

        for i in range(len(rows)):
            cnc_list.append(rows[i][0])


        for cnc in cnc_list:
            print (cnc)
            if (check_cnc_occurence_in_sample(cnc, cursor)):
                cursor.execute("UPDATE cnc SET cnc_in_sample = TRUE WHERE word_id = "+ str(cnc))
                connection.commit()
            else: 
                cursor.execute("UPDATE cnc SET cnc_in_sample = FALSE WHERE word_id = "+ str(cnc))
                connection.commit()


        # func test
        #print(check_cnc_occurence_in_sample(5584, cursor))  #1248905 true/1; 1193513 false/0

