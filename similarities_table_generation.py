# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

import cnc_tools
import word_vectors_tools as wvt

def create_similarities_table(cursor):

    cursor.execute("DROP TABLE IF EXISTS similarities;")
    
    cursor.execute("CREATE TABLE `similarities` ("+
      "`id` int(11) unsigned NOT NULL AUTO_INCREMENT,"+
      "`cnc_word_id` int(10) unsigned NOT NULL,"+
      "`comp_word_id` int(10) unsigned NOT NULL,"+
      "`sample_word_id` int(10) unsigned NOT NULL,"+
      "`cossim` float(32) NOT NULL,"+
      "PRIMARY KEY (`id`),"+
      "KEY `cnc_word_id` (`cnc_word_id`),"+
      "KEY `comp_word_id` (`comp_word_id`),"+
      "KEY `sample_word_id` (`sample_word_id`),"+
      "CONSTRAINT `word_vectors_ibfk_3` FOREIGN KEY (`cnc_word_id`) REFERENCES `word` (`id`),"+
      "CONSTRAINT `word_vectors_ibfk_4` FOREIGN KEY (`comp_word_id`) REFERENCES `word` (`id`),"+
      "CONSTRAINT `word_vectors_ibfk_5` FOREIGN KEY (`sample_word_id`) REFERENCES `word` (`id`)"+
    ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;")

def fill_similarities_table(cursor):

    cursor.execute("SELECT word_id FROM cnc WHERE solid_sample = TRUE")  

    rows = cursor.fetchall()
    cnc_list = []
    for i in range(len(rows)):
        cnc_list.append(int(rows[i][0]))

    for i in cnc_list:
        cnc = cnc_tools.get_cnc_dict(i, cursor)
        print(cnc)
        cnc = list(map(lambda x: x[0], cnc))

        # sample = cnc_tools.get_sample_dict(cnc[0], cursor)
        start = cnc_tools.get_sample_start_index(cnc[0], cursor)
        sample = list(range(start, cnc[0]))
        sample_vectors = []
        print("vectors")
        for s in sample:
            sample_vectors.append(wvt.get_word_vector(s, cursor))

        for comp in cnc:
            print(comp)
            comp_vector = wvt.get_word_vector(comp, cursor)
            # sims = []
            i = 0
            for sample_word in sample:
                sim = wvt.calc_similarity(comp_vector, sample_vectors[i])
                i = i + 1
                cursor.execute("INSERT INTO similarities (cnc_word_id, comp_word_id, sample_word_id, cossim) VALUES ("+
                    str(cnc[0])+", "+str(comp)+", "+str(sample_word)+", "+str(sim)+")")
            connection.commit()



if __name__ == '__main__':

    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()
        #create_similarities_table(cursor)
        fill_similarities_table(cursor)
