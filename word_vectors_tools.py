# this modul has functions for manipulating with word vectors and calculating similarities between vectors

import numpy as np

from scipy import spatial


def get_word_vector (index, cursor):
    cursor.execute("SELECT vector FROM word_vectors WHERE word_id = " + str (index))
    row = cursor.fetchone()       

    word_vector = np.frombuffer(row[0], dtype=np.float32)

    return word_vector

# 1 version: word vectors as an input
def calc_similarity (v1, v2):
    similarity = np.dot(v1, v2)/(np.linalg.norm(v1)*np.linalg.norm(v2))
    # similarity = 1 - spatial.distance.cosine(v1, v2) # with SciPy import
    return similarity

# 2 version: indexes of words as an input
def get_similarity (index1, index2, cursor):

    v1 = get_word_vector (index1, cursor)
    v2 = get_word_vector (index2, cursor)

    return calc_similarity(v1, v2)