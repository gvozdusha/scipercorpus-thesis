# this script calculates the size of preceding words sets per each cnc (5100)
# and adds this information to the cnc-table in the DB
# selected before threshold = 0.503 is used for the sets calculation 

# For MySQL connections
import mysql.connector
from mysql.connector import errorcode

# The database definition is in database_utils
from database_utils import mysql_get_connection

import cnc_tools


if __name__ == '__main__':

    threshold = 0.503
    
    connection = mysql_get_connection()
    with connection:
        cursor = connection.cursor()

        #add new column
        #cursor.execute("ALTER TABLE cnc ADD COLUMN preceeding_words int NOT NULL") 

        cursor.execute("SELECT word_id from cnc WHERE solid_sample is true")
        rows = cursor.fetchall()
        print (len(rows))

        for i in range(len(rows)):
            print (rows[i][0])
            words_number = len( cnc_tools.get_cnc_preceedings_set_by_threshold(rows[i][0], threshold, cursor) )
            #print (OKEY2)

            cursor.execute("UPDATE cnc SET preceeding_words = " + str(words_number)+ " WHERE word_id = "+ str(rows[i][0]))
            connection.commit()

        #print(cnc_tools.get_cnc_preceedings_set_by_threshold(1434, threshold, cursor))

